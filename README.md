## Test PHP Programmer

Membuat Aplikasi Peminjaman Buku Menggunakan PHP + Framework (Codeigniter,Laravel, atau Yii) dan Twitter Bootstrap. 
Fitur-fitur yang harus ada pada aplikasi, adalah sebagai berikut:

- Login/Logout Aplikasi dengan menggunakan session (Authentication)
- Master Table (CRUD)
    - Master table User (untuk password menggunakan MD5 + salt, string salt untuk masing masing user berbeda).
    - Master table Member.
    - Master table Book.
    
- Peminjaman Buku
    > Tampilkan berupa table seluruh buku yang dipinjam, ketika di klik baris akan masuk kedalam peminjaman buku. Field yang ada dalam form antara lain: judul buku, pengarang, isbn, tahun publikasi, nama peminjam (pilih), tanggal pinjam(input), tanggal kembali aktual (input).
    
- Pengembalian Buku
    > Tampilkan berupa table seluruh buku yang dipinjam, ketika di klik baris akan
    > masuk kedalam peminjaman buku. Field yang ada dalam form antara lain: judul 
    > buku, pengarang, isbn, tahun publikasi, nama peminjam, tanggal pinjam, tanggal
    > kembali aktual (input).
    
- Report – Peminjaman Buku
    > Tampilan berupa table peminjaman buku, field yang di tampilkan judul buku,
    > pengarang, isbn, tahun publikasi, tanggal pinjam, tanggal kembali, 
    > status (masih dipinjam/sudah balik), nama peminjam, total denda 
    > ( 1 hari keterlambatan didenda Rp.2000). Ketika di klik row jika
    > terpinjam tampilkan form “Pengembalikan Buku”, jika tidak terpinjam tampilkan 
    > form Peminjaman Buku

- Report – Semua Buku
    > Tampilan berupa table peminjaman buku, field yang di tampilkan judul buku, 
    > pengarang, isbn, tahun publikasi, status (terpinjam/tidak). Ketika di click row 
    > jika terpinjam tampilkan form “Pengembalikan Buku”, jika tidak terpinjam 
    > tampilkan form “Peminjaman Buku”

### Waktu Pengerjaan
Kerjakan dimulai dari yang paling mudah. Script dipush ke repo sebelum pukul 9.00 pagi
Kriteria penilaian antara lain: Logika, kreatifitas, dan solusi atas suatu masalah.